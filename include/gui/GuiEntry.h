#ifndef GUIENTRY_H
#define GUIENTRY_H

#include <string>

#include <SFML/Graphics.hpp>

class GuiEntry
{
	public:
		GuiEntry(const std::string& message, sf::RectangleShape shape, sf::Text text, bool enabled);
		virtual ~GuiEntry();

		sf::RectangleShape shape;
		std::string message;
		sf::Text text;
		bool enabled;
	protected:
	private:
};

#endif // GUIENTRY_H
