#ifndef GUI_H
#define GUI_H

#include <string>
#include <vector>
#include <utility>

#include <SFML/Graphics.hpp>

#include "GuiStyle.h"
#include "GuiEntry.h"

class Gui : public sf::Transformable, public sf::Drawable
{
	public:
		Gui(sf::Vector2f dimensions, int padding, bool horizontal,
			GuiStyle& style, std::vector<std::pair<std::string, std::string>> entries, std::vector<bool> entriesEnabled);
		virtual ~Gui();

		sf::Vector2f getSize();

		// return the entry that the mouse if over, returns -1 if mouse is outside gui
		int getEntry(const sf::Vector2f mousePos);

		// chaneg the text of an entry
		void setEntryText(int entry, std::string text);

		// change the entry dimensions
		void setDimensions(sf::Vector2f dimensions);

		// draw the menu
		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

		void show();

		void hide();

		// highlights the entry
		void highlight(const int entry);

//		returnt he message bound to the entry
		std::string activate(const int entry);
		std::string activate(const sf::Vector2f mousePos);

		bool horizontal;
		GuiStyle style;
		sf::Vector2f dimensions;
		int padding;
		std::vector<GuiEntry> entries;
		bool visible;
	protected:
	private:
		// if true entries will be horizontal not vertical

};

#endif // GUI_H
