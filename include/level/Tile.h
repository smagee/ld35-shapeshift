#ifndef TILE_H
#define TILE_H

#include <SFML/Graphics.hpp>

class Tile : public sf::Drawable, public sf::Transformable
{
	public:
		Tile(std::string name, int type, int width, int height, int x, int y, sf::Texture* texture);
		virtual ~Tile();

		bool contains(sf::Vector2i mousePos);
		void changeTile();
		void changeTile(std::string message);

		std::string name;
		int type;
		int width;
		int height;
		int posX;
		int posY;
		int texX;
		int texY;
	protected:
	private:
		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

		sf::Sprite tile;
		sf::Texture* texture;
};

#endif // TILE_H
