#ifndef GAME_H
#define GAME_H

#include <SFML/Graphics.hpp>

#include "GuiStyle.h"

class State;

class Game
{
	public:
		Game();
		virtual ~Game();

		void startGame();

		sf::RenderWindow* getWindow();
		void changeState(State* state);
		State* prevState;
		State* currentState;

		void loadStylesheets();
		std::map<std::string, GuiStyle> stylesheets;
		void loadFonts();
		std::map<std::string, sf::Font> fonts;
	protected:
	private:
		void initializeGame();
		void runGameLoop();

		sf::RenderWindow window;
};

#endif // GAME_H
