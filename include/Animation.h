#ifndef ANIMATION_H
#define ANIMATION_H

class Animation
{
	public:
		Animation(unsigned int startFrame, unsigned int endFrame, float duration);
		virtual ~Animation();

		unsigned int getLength();

		float duration;
	protected:
	private:
		unsigned int startFrame;
		unsigned int endFrame;
};

#endif // ANIMATION_H
