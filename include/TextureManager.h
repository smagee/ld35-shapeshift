#ifndef TEXTUREMANAGER_H
#define TEXTUREMANAGER_H

#include <SFML/Graphics.hpp>

class TextureManager
{
	public:
		TextureManager();
		virtual ~TextureManager();

		void loadTexture(const std::string& name, const std::string &filename);

		sf::Texture& getRef(const std::string& texture);
	protected:
	private:
		std::map<std::string, sf::Texture> textures;
};

#endif // TEXTUREMANAGER_H
