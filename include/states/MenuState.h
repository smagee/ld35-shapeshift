#ifndef MENUSTATE_H
#define MENUSTATE_H

#include "State.h"
#include "Gui.h"
#include "TextureManager.h"

class MenuState : public State
{
	public:
		MenuState(Game* game);
		virtual ~MenuState();

		void initState();
		void runState();
		void exitState();
	protected:
	private:
		void pollEvents();
		void display();

		TextureManager textureManager;
		sf::Sprite mainmenu;
		std::map<std::string, Gui> guiSystem;
		sf::View view;
};

#endif // MENUSTATE_H
