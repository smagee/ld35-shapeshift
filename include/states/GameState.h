#ifndef GAMESTATE_H
#define GAMESTATE_H

#include "State.h"
#include "TextureManager.h"
#include "TileMap.h"
#include "Gui.h"
#include "Tile.h"

class GameState : public State
{
	public:
		GameState(Game* game);
		virtual ~GameState();

		void initState();
		void runState();
		void exitState();
	protected:
	private:
		void pollEvents();
		void display();

		TextureManager textureManager;
		TileMap tileMap;

		sf::View gameView;
		std::map<std::string, Gui> guiSystem;
		sf::View guiView;

		Tile* selectedTile;
};

#endif // GAMESTATE_H
