#include "MenuState.h"
#include "GameState.h"
#include <utility>

MenuState::MenuState(Game* game)
{
	this->game = game;

	sf::Vector2f pos = sf::Vector2f(this->game->getWindow()->getSize());
	view.setSize(pos);
	pos *= 0.5f;
	view.setCenter(pos);

	guiSystem.emplace("MainMenu",
		Gui(sf::Vector2f(192, 32), 4, false, this->game->stylesheets.at("Button"),
		{
			std::make_pair("Start Game", "startGame"),
			std::make_pair("Empty 1", ""),
			std::make_pair("Exit Game", "exitGame")
		},
		{
			true,
			false,
			true
		}
		)
	);

	guiSystem.at("MainMenu").setPosition(pos);
	guiSystem.at("MainMenu").setOrigin(96, 32 * 1/2);
	guiSystem.at("MainMenu").show();

	textureManager.loadTexture("mainmenu", "assets/mainmenu/mainmenu.png");
}

MenuState::~MenuState()
{
	//dtor
}

void MenuState::initState()
{
	mainmenu.setTexture(textureManager.getRef("mainmenu"));
}

void MenuState::runState()
{
	pollEvents();
	display();
}

void MenuState::exitState()
{

}

void MenuState::pollEvents()
{
	sf::Event event;
	sf::Vector2f mousePos = game->getWindow()->mapPixelToCoords(sf::Mouse::getPosition(*(game->getWindow())), view);
	while(game->getWindow()->pollEvent(event))
	{
		switch(event.type)
		{
		case sf::Event::Closed:
		{
			game->getWindow()->close();
			break;
		}
		case sf::Event::Resized:
		{
			view.setSize(event.size.width, event.size.height);
			sf::Vector2f pos = sf::Vector2f(event.size.width, event.size.height);
			pos *= 0.5f;
			pos = game->getWindow()->mapPixelToCoords(sf::Vector2i(pos), view);
			guiSystem.at("MainMenu").setPosition(pos);
			break;
		}
		case sf::Event::MouseMoved:
		{
			guiSystem.at("MainMenu").highlight(guiSystem.at("MainMenu").getEntry(mousePos));

			break;
		}
		case sf::Event::MouseButtonPressed:
		{
			//click on menu items
			if(event.mouseButton.button == sf::Mouse::Left)
			{
				std::string msg = guiSystem.at("MainMenu").activate(mousePos);
				if(msg == "startGame")
					game->changeState(new GameState(game));
				if(msg == "exitGame")
					game->getWindow()->close();
			}
			break;
		}
		default:
			break;
		}
	}
}

void MenuState::display()
{
	game->getWindow()->setView(view);

	game->getWindow()->clear(sf::Color::Black);

	game->getWindow()->draw(mainmenu);

	for(auto gui : guiSystem)
		game->getWindow()->draw(gui.second);

	game->getWindow()->display();
}
