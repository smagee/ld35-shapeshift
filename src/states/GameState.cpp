#include "GameState.h"
#include "MenuState.h"

GameState::GameState(Game* game)
{
	this->game = game;

	sf::Vector2f pos = sf::Vector2f(this->game->getWindow()->getSize());
	gameView.setSize(pos);
	guiView.setSize(pos);
	pos *= 0.5f;
	gameView.setCenter(pos);
	guiView.setCenter(pos);

	guiSystem.emplace("TileMenu",
		Gui(sf::Vector2f(196, 16), 2, false, this->game->stylesheets.at("Button"),
		{
			std::make_pair("Head", "head"),
			std::make_pair("Chest", "chest"),
			std::make_pair("Waist", "waist"),
			std::make_pair("Left Arm", "left arm"),
			std::make_pair("Right Arm", "right arm"),
			std::make_pair("Left Leg", "left leg"),
			std::make_pair("Right leg", "right leg"),
		},
		{
			true,
			true,
			true,
			true,
			true,
			true,
			true,
		}
		)
	);
}

GameState::~GameState()
{
	//dtor
}

void GameState::initState()
{
	const int level[] =
	{
		0, 1, 2, 3, 4, 5, 6, 6, 7, 7, 7, 7, 6, 6, 5, 4, 3, 2, 1, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	};
	tileMap.load("assets/level/tiles.png", sf::Vector2u(32, 32), level, 20, 20);
}

void GameState::runState()
{
	pollEvents();
	display();
}

void GameState::exitState()
{

}

void GameState::pollEvents()
{
	sf::Event event;
	sf::Vector2f gameMousePos = game->getWindow()->mapPixelToCoords(sf::Mouse::getPosition(*(game->getWindow())), gameView);
	sf::Vector2f guiMousePos = game->getWindow()->mapPixelToCoords(sf::Mouse::getPosition(*(game->getWindow())), guiView);
	while(game->getWindow()->pollEvent(event))
	{
		switch(event.type)
		{
		case sf::Event::Closed:
			game->getWindow()->close();
			break;
		case sf::Event::MouseButtonPressed:
			if(event.mouseButton.button == sf::Mouse::Left)
			{
				if(guiSystem.at("TileMenu").visible == true)
				{
					std::string msg = guiSystem.at("TileMenu").activate(guiMousePos);
					selectedTile->changeTile(msg);
					guiSystem.at("TileMenu").hide();
				}
				else
				{
					Tile* t = tileMap.getTile(sf::Vector2i(gameMousePos));
					t->changeTile();
				}
			}

			if(event.mouseButton.button == sf::Mouse::Right)
			{
				selectedTile = tileMap.getTile(sf::Vector2i(gameMousePos));
				sf::Vector2f pos = guiMousePos;
				if(pos.x > game->getWindow()->getSize().x - guiSystem.at("TileMenu").getSize().x)
				{
					pos -= sf::Vector2f(guiSystem.at("TileMenu").getSize().x, 0);
				}
				if(pos.y > game->getWindow()->getSize().y - guiSystem.at("TileMenu").getSize().y)
				{
					pos -= sf::Vector2f(0, guiSystem.at("TileMenu").getSize().y);
				}

				guiSystem.at("TileMenu").setPosition(pos);
				guiSystem.at("TileMenu").show();
			}
			break;
		default:
			break;
		}
	}
}

void GameState::display()
{
	game->getWindow()->setView(gameView);
	game->getWindow()->clear(sf::Color::Black);
	game->getWindow()->draw(tileMap);

	game->getWindow()->setView(guiView);
	for(auto gui : guiSystem)
		game->getWindow()->draw(gui.second);

	game->getWindow()->display();
}
