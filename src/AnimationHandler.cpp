#include "AnimationHandler.h"

AnimationHandler::AnimationHandler()
{
	t = 0.0f;
	currentAnimation = -1;
}

AnimationHandler::AnimationHandler(const sf::IntRect& frameSize)
{
	this->frameSize = frameSize;
	t = 0.0f;
	currentAnimation = -1;
}

AnimationHandler::~AnimationHandler()
{
	//dtor
}

void AnimationHandler::AddAnimation(Animation& animation)
{
	animations.push_back(animation);
}

void AnimationHandler::update(const float dt)
{
	if(currentAnimation >= animations.size() || currentAnimation < 0)
		return;

	float duration = animations[currentAnimation].duration;

	// check if the animation has progressed to a new frame and if so change to the next frame
	if(int((t + dt) / duration) > int(t / duration))
	{
		//calulate frame number
		int frame = int((t + dt) / duration);

		frame %= animations[currentAnimation].getLength();

		sf::IntRect rect = frameSize;
		rect.left = rect.width * frame;
		rect.top = rect.height * currentAnimation;
		bounds = rect;
	}

	//increment time elasped
	t += dt;

	//adjust for looping
	if(t > duration * animations[currentAnimation].getLength())
		t = 0.0f;
}

void AnimationHandler::changeAnimation(unsigned int animation)
{
	// do not change the animation if the animation is currently active or the new animation does not exist
	if(currentAnimation == animation || animation >= animations.size() || animation < 0)
		return;

	currentAnimation = animation;

	sf::IntRect rect = frameSize;
	rect.top = rect.height * animation;
	bounds = rect;
	t = 0.0f;
}
