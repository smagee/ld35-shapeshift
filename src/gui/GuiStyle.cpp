#include "GuiStyle.h"

GuiStyle::GuiStyle()
{

}

GuiStyle::GuiStyle(
	sf::Font* font, float borderSize,
	sf::Color bodyColor, sf::Color borderColor, sf::Color textColor,
	sf::Color bodyHighlightColor, sf::Color borderHighlightColor, sf::Color textHighlightColor)
{
	this->bodyColor = bodyColor;
	this->borderColor = borderColor;
	this->textColor = textColor;
	this->bodyHighlightColor = bodyHighlightColor;
	this->borderHighlightColor = borderHighlightColor;
	this->textHighlightColor = textHighlightColor;
	this->font = font;
	this->borderSize = borderSize;
}

GuiStyle::~GuiStyle()
{
	//dtor
}
