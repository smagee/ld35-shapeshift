#include "GuiEntry.h"

GuiEntry::GuiEntry(const std::string& message, sf::RectangleShape shape, sf::Text text, bool enabled)
{
	this->message = message;
	this->shape = shape;
	this->text = text;
	this->enabled = enabled;
}

GuiEntry::~GuiEntry()
{
	//dtor
}
