#include "Gui.h"

#include <iostream>

Gui::Gui(
	sf::Vector2f dimensions, int padding, bool horizontal,
	GuiStyle& style, std::vector<std::pair<std::string, std::string>> entries, std::vector<bool> entriesEnabled
)
{
	this->visible = false;
	this->horizontal = horizontal;
	this->style = style;
	this->dimensions = dimensions;
	this->padding = padding;

	// construct the background shape
	sf::RectangleShape shape;
	shape.setSize(this->dimensions);
	shape.setFillColor(this->style.bodyColor);
	shape.setOutlineThickness(-this->style.borderSize);
	shape.setOutlineColor(this->style.borderColor);

	//construct each entry
	for(int i = 0; i < entries.size(); ++i)
	{
		sf::Text text;
		text.setString(entries[i].first);
		text.setFont(*this->style.font);
		text.setColor(this->style.textColor);
		text.setCharacterSize(this->dimensions.y - this->style.borderSize - this->padding);

		this->entries.push_back(GuiEntry(entries[i].second, shape, text, entriesEnabled[i]));
	}
}

Gui::~Gui()
{
	//dtor
}

sf::Vector2f Gui::getSize()
{
	return sf::Vector2f(dimensions.x, dimensions.y * entries.size());
}

int Gui::getEntry(const sf::Vector2f mousePos)
{
	if(entries.size() == 0)
		return -1;
	if(!visible)
		return -1;

	for(int i = 0; i < entries.size(); ++i)
	{
		// Translate point to use the entry local coordinates
		sf::Vector2f point = mousePos;
		point += entries[i].shape.getOrigin();
		point -= entries[i].shape.getPosition();

		if(point.x < 0 || point.x > entries[i].shape.getScale().x * dimensions.x)
			continue;
		if(point.y < 0 || point.y > entries[i].shape.getScale().y * dimensions.y)
			continue;
		return i;
	}
	return -1;
}

void Gui::setEntryText(int entry, std::string text)
{
	if(entry >= entries.size() || entry < 0)
		return;

	entries[entry].text.setString(text);
}

void Gui::setDimensions(sf::Vector2f dimensions)
{
	this->dimensions = dimensions;

	for(auto& entry : entries)
	{
		entry.shape.setSize(this->dimensions);
		entry.text.setCharacterSize(this->dimensions.y - style.borderSize - padding);
	}
}

void Gui::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	if(!visible)
		return;

	//draw each entry
	for(auto entry : entries)
	{
		if(entry.enabled)
		{
			target.draw(entry.shape);
			target.draw(entry.text);
		}
	}
}

void Gui::show()
{
	sf::Vector2f offset(0.0f, 0.0f);

	visible = true;

	for(auto& entry : entries)
	{
		// set origin
		sf::Vector2f origin = getOrigin();
		origin -= offset;
		entry.shape.setOrigin(origin);
		entry.text.setOrigin(origin);

		// set position
		entry.shape.setPosition(getPosition());
		entry.text.setPosition(getPosition());

		if(horizontal)
			offset.x += dimensions.x;
		else
			offset.y += dimensions.y;
	}
}

void Gui::hide()
{
	visible = false;
}

void Gui::highlight(const int entry)
{
	for(int i = 0; i < entries.size(); ++i)
	{
		if(i == entry)
		{
			entries[i].shape.setFillColor(style.bodyHighlightColor);
			entries[i].shape.setOutlineColor(style.borderHighlightColor);
			entries[i].text.setColor(style.textHighlightColor);
		}
		else
		{
			entries[i].shape.setFillColor(style.bodyColor);
			entries[i].shape.setOutlineColor(style.borderColor);
			entries[i].text.setColor(style.textColor);
		}
	}
}

std::string Gui::activate(const int entry)
{
	if(entry == -1)
		return "null";
	return entries[entry].message;
}

std::string Gui::activate(const sf::Vector2f mousePos)
{
	int entry = getEntry(mousePos);
	return activate(entry);
}
