#include "Tile.h"

Tile::Tile(std::string name, int type, int width, int height, int x, int y, sf::Texture* texture)
{
	this->name = name;
	this->type = type;
	this->width = width;
	this->height = height;
	this->posX = x;
	this->posY = y;
	this->texture = texture;
	this->texX = this->type % (texture->getSize().x / width);
	this->texY = this->type / (texture->getSize().x / width);
	this->tile.setTexture(*this->texture);
	this->tile.setTextureRect(sf::IntRect(texX * this->width, texY * this->height, this->width, this->height));
	this->tile.setPosition(this->posX, this->posY);
}

Tile::~Tile()
{
	//dtor
}

bool Tile::contains(sf::Vector2i mousePos)
{
	return tile.getGlobalBounds().contains(mousePos.x, mousePos.y);
}

void Tile::changeTile()
{
    if(type < 6)
        ++type;
    else
        type = 0;
    texX = type % (texture->getSize().x / width);
    texY = type / (texture->getSize().x / width);
    tile.setTextureRect(sf::IntRect(texX * width, texY * height, width, height));
}

void Tile::changeTile(std::string message)
{
	if(message == "head")
		type = 0;
	if(message == "chest")
		type = 1;
	if(message == "waist")
		type = 2;
	if(message == "left arm")
		type = 3;
	if(message == "right arm")
		type = 4;
	if(message == "left leg")
		type = 5;
	if(message == "right leg")
		type = 6;

	texX = type % (texture->getSize().x / width);
	texY = type / (texture->getSize().x / width);
	tile.setTextureRect(sf::IntRect(texX * width, texY * height, width, height));
}

void Tile::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	states.texture = texture;
	target.draw(tile, states);
}
